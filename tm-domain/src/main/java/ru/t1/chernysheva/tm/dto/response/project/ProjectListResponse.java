package ru.t1.chernysheva.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.chernysheva.tm.dto.response.user.AbstractUserResponse;
import ru.t1.chernysheva.tm.model.Project;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ProjectListResponse extends AbstractProjectResponse {

    private List<Project> projects;

    public ProjectListResponse(List<Project> projects) {
        this.projects = projects;
    }

}
