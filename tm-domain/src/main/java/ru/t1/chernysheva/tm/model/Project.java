package ru.t1.chernysheva.tm.model;

import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.api.model.IWBS;
import ru.t1.chernysheva.tm.enumerated.Status;
import lombok.*;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractUserOwnedModel implements IWBS {

    private static final long serialVersionUID = 1;

    @NonNull
    private String name = "";

    @Nullable
    private Status status = Status.NOT_STARTED;

    @Nullable
    private String description = "";

    @Nullable
    private Date created = new Date();

    public Project(String name, Status status) {
        this.name = name;
        this.status = status;
    }
}
