package ru.t1.chernysheva.tm.model;

import lombok.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.enumerated.Role;

import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class User extends AbstractModel {

    private static final long serialVersionUID = 1;

    @NonNull
    private String login;

    @NonNull
    private String passwordHash;

    @Nullable
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    @NonNull
    private Role role = Role.USUAL;

    @NonNull
    private Boolean locked = false;

    @NotNull
    public Boolean isLocked() {
        return locked;
    }

}
