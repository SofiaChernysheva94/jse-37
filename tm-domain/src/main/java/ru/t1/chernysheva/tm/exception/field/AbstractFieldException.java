package ru.t1.chernysheva.tm.exception.field;

import ru.t1.chernysheva.tm.exception.AbstractException;

public class AbstractFieldException extends AbstractException {

    public AbstractFieldException() {
    }

    public AbstractFieldException(final String message) {
        super(message);
    }

    public AbstractFieldException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AbstractFieldException(final Throwable cause) {
        super(cause);
    }

    public AbstractFieldException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
