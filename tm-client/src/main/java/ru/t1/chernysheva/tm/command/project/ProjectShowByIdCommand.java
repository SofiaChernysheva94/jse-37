package ru.t1.chernysheva.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.request.project.ProjectShowByIdRequest;
import ru.t1.chernysheva.tm.model.Project;
import ru.t1.chernysheva.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends  AbstractProjectCommand {

    @NotNull
    private final String NAME = "project-show-by-id";

    @NotNull
    private final String DESCRIPTION = "Show project by Id.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();

        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest(getToken());
        request.setId(id);

        @Nullable final Project project = getProjectEndpoint().showProjectById(request).getProject();
        showProject(project);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
