package ru.t1.chernysheva.tm.command.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.dto.request.user.UserLockRequest;
import ru.t1.chernysheva.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "user-lock";

    @NotNull
    private final String DESCRIPTION = "User lock.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[USER LOCK]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();

        @NotNull final UserLockRequest request = new UserLockRequest();
        request.setLogin(login);

        getUserEndpoint().lockUser(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
