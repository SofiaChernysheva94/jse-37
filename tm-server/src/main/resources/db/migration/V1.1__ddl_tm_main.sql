CREATE SCHEMA IF NOT EXISTS tm;

CREATE TABLE IF NOT EXISTS tm.tm_project
(
    id text COLLATE pg_catalog."default",
    created time with time zone,
    user_id text COLLATE pg_catalog."default",
    description text COLLATE pg_catalog."default",
    status text COLLATE pg_catalog."default",
    name text COLLATE pg_catalog."default"
)

    TABLESPACE pg_default;

ALTER TABLE IF EXISTS tm.tm_project
    OWNER to postgres;


CREATE TABLE IF NOT EXISTS tm.tm_session
(
    id text COLLATE pg_catalog."default",
    date date,
    date_test date,
    user_id text COLLATE pg_catalog."default",
    role text COLLATE pg_catalog."default",
    session_date date
)

    TABLESPACE pg_default;

ALTER TABLE IF EXISTS tm.tm_session
    OWNER to postgres;


CREATE TABLE IF NOT EXISTS tm.tm_task
(
    id text COLLATE pg_catalog."default",
    created time with time zone,
    user_id text COLLATE pg_catalog."default",
    description text COLLATE pg_catalog."default",
    status text COLLATE pg_catalog."default",
    name text COLLATE pg_catalog."default",
    project_id text COLLATE pg_catalog."default"
)

    TABLESPACE pg_default;

ALTER TABLE IF EXISTS tm.tm_task
    OWNER to postgres;

CREATE TABLE IF NOT EXISTS tm.tm_user
(
    passw text COLLATE pg_catalog."default",
    login text COLLATE pg_catalog."default",
    id text COLLATE pg_catalog."default",
    email text COLLATE pg_catalog."default",
    password_hash text COLLATE pg_catalog."default",
    first_name text COLLATE pg_catalog."default",
    last_name text COLLATE pg_catalog."default",
    middle_name text COLLATE pg_catalog."default",
    locked boolean,
    role text COLLATE pg_catalog."default"
)

    TABLESPACE pg_default;

ALTER TABLE IF EXISTS tm.tm_user
    OWNER to postgres;

