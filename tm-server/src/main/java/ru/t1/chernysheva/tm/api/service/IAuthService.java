package ru.t1.chernysheva.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.enumerated.Role;
import ru.t1.chernysheva.tm.model.Session;
import ru.t1.chernysheva.tm.model.User;

public interface IAuthService {

    @NotNull
    Session validateToken(@Nullable String token) throws Exception;

    void invalidate(@Nullable Session session) throws Exception;

    @NotNull
    User registry(@Nullable String login, @Nullable String password, @Nullable String email) throws Exception;

    String login(@Nullable String login, @Nullable String password) throws Exception;

}
