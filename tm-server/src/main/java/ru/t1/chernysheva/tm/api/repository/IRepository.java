package ru.t1.chernysheva.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.model.AbstractModel;

import java.sql.Connection;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IRepository<M extends AbstractModel> {

    void setRepositoryConnection(Connection connection);

    interface IRepositoryOptional<M extends AbstractModel> {

        Optional<M> findOneById(String id) throws Exception;

        Optional<M> findOneByIndex(Integer index) throws Exception;

    }

    default IRepositoryOptional<M> optional() {
        return new IRepositoryOptional<M>() {
            @Override
            public Optional<M> findOneById(final String id) throws Exception {
                return Optional.ofNullable(IRepository.this.findOneById(id));
            }

            @Override
            public Optional<M> findOneByIndex(final Integer index) throws Exception {
                return Optional.ofNullable(IRepository.this.findOneByIndex(index));
            }
        };
    }

    void clear() throws Exception;

    @NotNull
    List<M> findAll() throws Exception;

    @NotNull
    List<M> findAll(@Nullable Comparator<M> comparator) throws Exception;

    @Nullable
    M add(@NotNull M model) throws Exception;

    @NotNull
    Collection<M> add(@NotNull Collection<M> models) throws Exception;

    @NotNull
    Collection<M> set(@NotNull Collection<M> models) throws Exception;

    boolean existsById(@NotNull String id) throws Exception;

    @Nullable
    M findOneById(@NotNull String id) throws Exception;

    @Nullable
    M findOneByIndex(@Nullable Integer index) throws Exception;

    int getSize() throws Exception;

    @Nullable
    M remove(@Nullable M model) throws Exception;

    @Nullable
    M removeById(@NotNull String id) throws Exception;

    @Nullable
    M removeByIndex(@Nullable Integer index) throws Exception;

    void removeAll(@Nullable Collection<M> collection) throws Exception;

}
