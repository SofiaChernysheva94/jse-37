package ru.t1.chernysheva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.api.repository.IRepository;
import ru.t1.chernysheva.tm.api.service.IConnectionService;
import ru.t1.chernysheva.tm.api.service.IService;
import ru.t1.chernysheva.tm.enumerated.Sort;
import ru.t1.chernysheva.tm.exception.entity.EntityNotFoundException;
import ru.t1.chernysheva.tm.exception.field.IdEmptyException;
import ru.t1.chernysheva.tm.exception.field.IndexIncorrectException;
import ru.t1.chernysheva.tm.model.AbstractModel;

import java.sql.Connection;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final R repository;

    @NotNull
    protected IConnectionService connectionService;

    @NotNull
    protected Connection getConnection() {
        return connectionService.getConnection();
    }

    public AbstractService(@NotNull final R repository, @NotNull IConnectionService connectionService) {
        this.repository = repository;
        this.connectionService = connectionService;
    }

    @Override
    public void clear() throws Exception {
        @NotNull Connection connection = getConnection();
        try {
            repository.setRepositoryConnection(connection);
            repository.clear();
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public List<M> findAll() throws Exception {
        @NotNull Connection connection = getConnection();
        @NotNull List<M> result;
        try {
            repository.setRepositoryConnection(connection);
            result = repository.findAll();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) throws Exception {
        @NotNull Connection connection = getConnection();
        @NotNull List<M> result;
        try {
            repository.setRepositoryConnection(connection);
            if (comparator == null) result = findAll();
            else result = repository.findAll(comparator);
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @NotNull
    @SuppressWarnings("unchecked")
    @Override
    public List<M> findAll(@Nullable final Sort sort) throws Exception {
        @NotNull Connection connection = getConnection();
        @NotNull List<M> result;
        try {
            repository.setRepositoryConnection(connection);
            if (sort == null) result = findAll();
            else result = repository.findAll(sort.getComparator());
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    public M add(@Nullable final M model) throws Exception {
        if (model == null) throw new EntityNotFoundException();
        @NotNull Connection connection = getConnection();
        try {
            repository.setRepositoryConnection(connection);
            repository.add(model);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull Collection<M> models) throws Exception {
        if (models.isEmpty()) return Collections.emptyList();
        @NotNull Connection connection = getConnection();
        @NotNull Collection<M> result;
        try {
            repository.setRepositoryConnection(connection);
            result = repository.add(models);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull Collection<M> models) throws Exception {
        if (models.isEmpty()) return Collections.emptyList();
        @NotNull Connection connection = getConnection();
        @NotNull Collection<M> result;
        try {
            repository.setRepositoryConnection(connection);
            result = repository.set(models);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @Override
    public boolean existsById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return false;
        @NotNull Connection connection = getConnection();
        boolean result;
        try {
            repository.setRepositoryConnection(connection);
            result = repository.existsById(id);
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        @NotNull Connection connection = getConnection();
        M result;
        try {
            repository.setRepositoryConnection(connection);
            result = repository.findOneById(id);
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) throws Exception {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull Connection connection = getConnection();
        M result;
        try {
            repository.setRepositoryConnection(connection);
            result = repository.findOneByIndex(index);
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @Override
    public int getSize() throws Exception {
        @NotNull Connection connection = getConnection();
        int result;
        try {
            repository.setRepositoryConnection(connection);
            result = repository.getSize();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    public M remove(@Nullable final M model) throws Exception {
        if (model == null) throw new EntityNotFoundException();
        @NotNull Connection connection = getConnection();
        M result;
        try {
            repository.setRepositoryConnection(connection);
            result = repository.remove(model);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull Connection connection = getConnection();
        M result;
        try {
            repository.setRepositoryConnection(connection);
            result = repository.removeById(id);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final Integer index) throws Exception {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull Connection connection = getConnection();
        M result;
        try {
            repository.setRepositoryConnection(connection);
            result = repository.removeByIndex(index);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @Override
    public void removeAll(@Nullable final Collection<M> collection) throws Exception {
        if (collection == null) return;
        @NotNull Connection connection = getConnection();
        try {
            repository.setRepositoryConnection(connection);
            repository.removeAll(collection);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
