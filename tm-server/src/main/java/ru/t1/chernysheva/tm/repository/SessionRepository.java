package ru.t1.chernysheva.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.api.repository.ISessionRepository;
import ru.t1.chernysheva.tm.enumerated.DBColumns;
import ru.t1.chernysheva.tm.enumerated.DBTables;
import ru.t1.chernysheva.tm.model.Session;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    @Override
    @NotNull
    protected String getTableName() {
        return DBTables.TM_SESSION.name();
    }

    @Override
    @NotNull
    @SneakyThrows
    public Session fetch(@NotNull final ResultSet row) {
        @NotNull final Session session = new Session();
        session.setId(row.getString(DBColumns.ID_COLUMN.getColumnName()));
        session.setUserId(row.getString(DBColumns.USER_ID_COLUMN.getColumnName()));
        return session;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session add(@NotNull final Session model) {
        @NotNull final String query = String.format(
                "INSERT INTO %s (%s, %s, %s, %s) VALUES (?, ?, ?, ?)",
                DBTables.TM_SESSION.name(), DBColumns.ID_COLUMN.getColumnName(),
                DBColumns.USER_ID_COLUMN.getColumnName(), DBColumns.SESSION_DATE_COLUMN.getColumnName(),
                DBColumns.ROLE_COLUMN.getColumnName());
        try (@NotNull PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, model.getId());
            statement.setString(2, model.getUserId());
            statement.setTimestamp(3, new Timestamp(model.getDate().getTime()));
            statement.setString(4, model.getRole().name());
            statement.executeUpdate();
        }
        return model;
    }

}
