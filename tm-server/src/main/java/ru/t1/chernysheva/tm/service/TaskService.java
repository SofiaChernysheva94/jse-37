package ru.t1.chernysheva.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.api.service.IConnectionService;
import ru.t1.chernysheva.tm.api.service.ITaskService;
import ru.t1.chernysheva.tm.api.repository.ITaskRepository;
import ru.t1.chernysheva.tm.enumerated.Status;
import ru.t1.chernysheva.tm.exception.entity.TaskNotFoundException;
import ru.t1.chernysheva.tm.exception.field.*;
import ru.t1.chernysheva.tm.model.Task;

import java.sql.Connection;
import java.util.Collections;
import java.util.List;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@NotNull final ITaskRepository repository, @NotNull IConnectionService connectionService) {
        super(repository, connectionService);
    }

    @Override
    public void setRepositoryConnection(Connection connection) {
        repository.setRepositoryConnection(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @NotNull final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull Connection connection = getConnection();
        @Nullable final Task task;
        try {
            repository.setRepositoryConnection(connection);
            task = repository.findOneById(userId, id);
            if (task == null) throw new TaskNotFoundException();
            if (status == null) throw new StatusEmptyException();
            task.setStatus(status);
            repository.update(task);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task changeTaskStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable  final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull Connection connection = getConnection();
        @Nullable final Task task;
        try {
            repository.setRepositoryConnection(connection);
            task = repository.findOneByIndex(userId, index);
            if (task == null) throw new TaskNotFoundException();
            if (status == null) throw new StatusEmptyException();
            task.setStatus(status);
            repository.update(task);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Connection connection = getConnection();
        @Nullable Task task = new Task();
        try {
            repository.setRepositoryConnection(connection);
            task.setName(name);
            if (description != null && !description.isEmpty())
                task.setDescription(description);
            task = repository.add(userId, task);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Connection connection = getConnection();
        @Nullable Task task = new Task();
        try {
            repository.setRepositoryConnection(connection);
            task.setName(name);
            task = repository.add(userId, task);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @NotNull Connection connection = getConnection();
        @Nullable final List<Task> result;
        try {
            repository.setRepositoryConnection(connection);
            result = repository.findAllByProjectId(userId, projectId);
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Connection connection = getConnection();
        @Nullable Task task;
        try {
            repository.setRepositoryConnection(connection);
            task = repository.findOneById(userId, id);
            if (task == null) throw new TaskNotFoundException();
            task.setName(name);
            if (description != null && !description.isEmpty())
                task.setDescription(description);
            task = repository.update(task);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Connection connection = getConnection();
        @Nullable Task task;
        try {
            repository.setRepositoryConnection(connection);
            task = repository.findOneByIndex(userId, index);
            if (task == null) throw new TaskNotFoundException();
            task.setName(name);
            if (description != null && !description.isEmpty())
                task.setDescription(description);
            task = repository.update(task);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

}
