package ru.t1.chernysheva.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task>, IUserOwnedRepository<Task> {

    @Nullable
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId) throws Exception;

    @NotNull
    Task update(@NotNull Task model) throws Exception;

}
