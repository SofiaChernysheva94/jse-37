package ru.t1.chernysheva.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.enumerated.Status;
import ru.t1.chernysheva.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

    @Nullable Project updateById(@Nullable String userId,
                                 @Nullable String id,
                                 @Nullable String name,
                                 @Nullable String description
    ) throws Exception;

    @Nullable Project updateByIndex(@Nullable String userId,
                                    @Nullable Integer index,
                                    @Nullable String name,
                                    @Nullable String description
    ) throws Exception;

    Project changeProjectStatusById(@Nullable String userId,
                                    @Nullable String Id,
                                    @Nullable Status status
    ) throws Exception;

    Project changeProjectStatusByIndex(@Nullable String userId,
                                       @Nullable Integer index,
                                       @Nullable Status status
    ) throws Exception;

    @Nullable
    Project create(@Nullable String userId, @Nullable String name) throws Exception;

    @Nullable
    Project create(@Nullable String userId,
                   @Nullable String name,
                   @Nullable String description
    ) throws Exception;

}
