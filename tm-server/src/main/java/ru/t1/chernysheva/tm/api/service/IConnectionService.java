package ru.t1.chernysheva.tm.api.service;

import lombok.SneakyThrows;

import javax.validation.constraints.NotNull;
import java.sql.Connection;

public interface IConnectionService {

    @NotNull
    @SneakyThrows
    Connection getConnection();

}
