package ru.t1.chernysheva.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.api.service.IConnectionService;
import ru.t1.chernysheva.tm.api.service.IProjectService;
import ru.t1.chernysheva.tm.api.repository.IProjectRepository;
import ru.t1.chernysheva.tm.exception.entity.ProjectNotFoundException;
import ru.t1.chernysheva.tm.exception.field.*;
import ru.t1.chernysheva.tm.model.Project;
import ru.t1.chernysheva.tm.enumerated.Status;

import java.sql.Connection;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull final IProjectRepository repository, @NotNull IConnectionService connectionService) {
        super(repository, connectionService);
    }

    @Override
    public void setRepositoryConnection(Connection connection) {
        repository.setRepositoryConnection(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull Connection connection = getConnection();
        @Nullable final Project project;
        try {
            repository.setRepositoryConnection(connection);
            project = repository.findOneById(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            if (status == null) throw new StatusEmptyException();
            project.setStatus(status);
            repository.update(project);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project changeProjectStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull Connection connection = getConnection();
        @Nullable final Project project;
        try {
            repository.setRepositoryConnection(connection);
            project = repository.findOneByIndex(userId, index);
            if (project == null) throw new ProjectNotFoundException();
            if (status == null) throw new StatusEmptyException();
            project.setStatus(status);
            repository.update(project);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return project;
    }

    @Nullable
    @Override
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final Project project = new Project();
        project.setName(name);
        if (description != null && !description.isEmpty())
            project.setDescription(description);
        return add(userId, project);
    }

    @Nullable
    @Override
    public Project create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final Project project = new Project();
        project.setName(name);
        return add(userId, project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Connection connection = getConnection();
        @Nullable final Project project;
        try {
            repository.setRepositoryConnection(connection);
            project = repository.findOneById(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            project.setName(name);
            if (description != null && !description.isEmpty())
                project.setDescription(description);
            repository.update(project);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Connection connection = getConnection();
        @Nullable final Project project;
        try {
            repository.setRepositoryConnection(connection);
            project = repository.findOneByIndex(userId, index);
            if (project == null) throw new ProjectNotFoundException();
            project.setName(name);
            if (description != null && !description.isEmpty())
                project.setDescription(description);
            repository.update(project);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return project;
    }

}