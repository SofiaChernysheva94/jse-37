package ru.t1.chernysheva.tm.service;

import lombok.SneakyThrows;
import ru.t1.chernysheva.tm.api.service.IConnectionService;
import ru.t1.chernysheva.tm.api.service.IPropertyService;

import javax.validation.constraints.NotNull;
import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    public ConnectionService(@NotNull IPropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Connection getConnection() {
        final String username = propertyService.getDBUser();
        final String password = propertyService.getDBPassword();
        final String url = propertyService.getDBUrl() + propertyService.getDbSchema();
        final Connection connection = DriverManager.getConnection(url, username, password);
        connection.setAutoCommit(false);
        return connection;
    }

}
