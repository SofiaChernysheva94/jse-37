package ru.t1.chernysheva.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.api.endpoint.ISystemEndpoint;
import ru.t1.chernysheva.tm.api.service.IPropertyService;
import ru.t1.chernysheva.tm.api.service.IServiceLocator;
import ru.t1.chernysheva.tm.dto.request.ApplicationAboutRequest;
import ru.t1.chernysheva.tm.dto.request.ApplicationVersionRequest;
import ru.t1.chernysheva.tm.dto.response.ApplicationAboutResponse;
import ru.t1.chernysheva.tm.dto.response.ApplicationVersionResponse;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.chernysheva.tm.api.endpoint.ISystemEndpoint")
public final class SystemEndpoint implements ISystemEndpoint {

    @NotNull
    private final IServiceLocator serviceLocator;

    public SystemEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    @WebMethod
    public ApplicationAboutResponse getAbout(@NotNull final ApplicationAboutRequest request) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ApplicationAboutResponse response = new ApplicationAboutResponse();
        response.setAuthorEmail(propertyService.getAuthorEmail());
        response.setAuthorName(propertyService.getAuthorName());
        return response;
    }

    @NotNull
    @Override
    @WebMethod
    public ApplicationVersionResponse getVersion(@NotNull final ApplicationVersionRequest request) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ApplicationVersionResponse response = new ApplicationVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

}
