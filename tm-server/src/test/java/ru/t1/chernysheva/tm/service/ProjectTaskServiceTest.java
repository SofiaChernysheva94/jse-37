package ru.t1.chernysheva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chernysheva.tm.api.repository.IProjectRepository;
import ru.t1.chernysheva.tm.api.repository.ITaskRepository;
import ru.t1.chernysheva.tm.api.service.*;
import ru.t1.chernysheva.tm.exception.entity.ProjectNotFoundException;
import ru.t1.chernysheva.tm.exception.entity.TaskNotFoundException;
import ru.t1.chernysheva.tm.exception.entity.*;
import ru.t1.chernysheva.tm.exception.field.ProjectIdEmptyException;
import ru.t1.chernysheva.tm.exception.field.TaskIdEmptyException;
import ru.t1.chernysheva.tm.exception.field.UserIdEmptyException;
import ru.t1.chernysheva.tm.marker.UnitCategory;
import ru.t1.chernysheva.tm.repository.ProjectRepository;
import ru.t1.chernysheva.tm.repository.TaskRepository;

import static ru.t1.chernysheva.tm.constant.ProjectTestData.*;
import static ru.t1.chernysheva.tm.constant.TaskTestData.*;
import static ru.t1.chernysheva.tm.constant.UserTestData.USER_TEST;


@Category(UnitCategory.class)
public final class ProjectTaskServiceTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectTaskService service = new ProjectTaskService(projectRepository, taskRepository, connectionService);

   /* @Before
    public void before() {
        projectRepository.add(USER_PROJECT1);
        projectRepository.add(USER_PROJECT2);
        taskRepository.add(USER_TASK1);
        taskRepository.add(USER_TASK2);
    }

    @After
    public void after() {
        taskRepository.removeAll(TASK_LIST);
        projectRepository.removeAll(PROJECT_LIST);
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.bindTaskToProject(null, USER_PROJECT1.getId(), USER_TASK1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.bindTaskToProject("", USER_PROJECT1.getId(), USER_TASK1.getId());
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.bindTaskToProject(USER_TEST.getId(), null, USER_TASK1.getId());
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.bindTaskToProject(USER_TEST.getId(), "", USER_TASK1.getId());
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            service.bindTaskToProject(USER_TEST.getId(), USER_PROJECT1.getId(), null);
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            service.bindTaskToProject(USER_TEST.getId(), USER_PROJECT1.getId(), "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.bindTaskToProject(USER_TEST.getId(), NON_EXISTING_PROJECT_ID, USER_TASK1.getId());
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            service.bindTaskToProject(USER_TEST.getId(), USER_PROJECT1.getId(), NON_EXISTING_TASK_ID);
        });
        service.bindTaskToProject(USER_TEST.getId(), USER_PROJECT2.getId(), USER_TASK1.getId());
        Assert.assertEquals(USER_PROJECT2.getId(), USER_TASK1.getProjectId());
        service.bindTaskToProject(USER_TEST.getId(), USER_PROJECT1.getId(), USER_TASK1.getId());
    }

    @Test
    public void removeProjectById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeProjectById(null, USER_PROJECT1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeProjectById("", USER_PROJECT1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.removeProjectById(USER_TEST.getId(), null);
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.removeProjectById(USER_TEST.getId(), "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.removeProjectById(USER_TEST.getId(), NON_EXISTING_PROJECT_ID);
        });
        service.bindTaskToProject(USER_TEST.getId(), USER_PROJECT1.getId(), USER_TASK1.getId());
        service.bindTaskToProject(USER_TEST.getId(), USER_PROJECT1.getId(), USER_TASK2.getId());
        service.removeProjectById(USER_TEST.getId(), USER_PROJECT1.getId());
        Assert.assertNull(projectRepository.findOneById(USER_PROJECT1.getId()));
        Assert.assertNull(taskRepository.findOneById(USER_TASK1.getId()));
        Assert.assertNull(taskRepository.findOneById(USER_TASK2.getId()));
    }

    @Test
    public void unbindTaskFromProject() {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.unbindTaskFromProject(null, USER_PROJECT1.getId(), USER_TASK1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.unbindTaskFromProject("", USER_PROJECT1.getId(), USER_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.unbindTaskFromProject(USER_TEST.getId(), null, USER_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.unbindTaskFromProject(USER_TEST.getId(), "", USER_TASK1.getId());
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            service.unbindTaskFromProject(USER_TEST.getId(), USER_PROJECT1.getId(), null);
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            service.unbindTaskFromProject(USER_TEST.getId(), USER_PROJECT1.getId(), "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.unbindTaskFromProject(USER_TEST.getId(), NON_EXISTING_PROJECT_ID, USER_TASK1.getId());
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            service.unbindTaskFromProject(USER_TEST.getId(), USER_PROJECT1.getId(), NON_EXISTING_TASK_ID);
        });
        service.unbindTaskFromProject(USER_TEST.getId(), USER_PROJECT1.getId(), USER_TASK1.getId());
        Assert.assertNull(USER_TASK1.getProjectId());
        service.bindTaskToProject(USER_TEST.getId(), USER_PROJECT1.getId(), USER_TASK1.getId());
    }
*/
}
