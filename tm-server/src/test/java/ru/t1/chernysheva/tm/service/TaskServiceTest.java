package ru.t1.chernysheva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chernysheva.tm.api.repository.ITaskRepository;
import ru.t1.chernysheva.tm.api.service.*;
import ru.t1.chernysheva.tm.comparator.NameComparator;
import ru.t1.chernysheva.tm.enumerated.Sort;
import ru.t1.chernysheva.tm.enumerated.Status;
import ru.t1.chernysheva.tm.exception.entity.EntityNotFoundException;
import ru.t1.chernysheva.tm.exception.entity.TaskNotFoundException;
import ru.t1.chernysheva.tm.exception.entity.*;
import ru.t1.chernysheva.tm.exception.field.*;
import ru.t1.chernysheva.tm.marker.UnitCategory;
import ru.t1.chernysheva.tm.model.Task;
import ru.t1.chernysheva.tm.repository.TaskRepository;

import java.util.*;
import java.util.stream.Collectors;

import static ru.t1.chernysheva.tm.constant.SessionTestData.*;
import static ru.t1.chernysheva.tm.constant.TaskTestData.*;
import static ru.t1.chernysheva.tm.constant.ProjectTestData.*;
import static ru.t1.chernysheva.tm.constant.UserTestData.ADMIN_TEST;
import static ru.t1.chernysheva.tm.constant.UserTestData.USER_TEST;


@Category(UnitCategory.class)
public final class TaskServiceTest {

    @NotNull
    private final ITaskRepository repository = new TaskRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ITaskService service = new TaskService(repository, connectionService);

/*    @Before
    public void before() {
        repository.add(USER_TASK1);
        repository.add(USER_TASK2);
    }

    @After
    public void after() {
        repository.removeAll(TASK_LIST);
    }

    @Test
    public void add() {
        Assert.assertNull(service.add(NULL_TASK));
        Assert.assertNotNull(service.add(ADMIN_TASK1));
        @Nullable final Task task = service.findOneById(ADMIN_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(ADMIN_TASK1, task);
    }

    @Test
    public void addByUserId() {
        Assert.assertNull(service.add(ADMIN_TEST.getId(), NULL_TASK));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.add(null, ADMIN_TASK1);
        });
        Assert.assertNotNull(service.add(ADMIN_TEST.getId(), ADMIN_TASK1));
        @Nullable final Task task = service.findOneById(ADMIN_TEST.getId(), ADMIN_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(ADMIN_TASK1, task);
    }

    @Test
    public void addMany() {
        Assert.assertNotNull(service.add(ADMIN_TASK_LIST));
        for (final Task task : ADMIN_TASK_LIST)
            Assert.assertEquals(task, service.findOneById(task.getId()));
    }

    @Test
    public void set() {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        @NotNull final ITaskService emptyService = new TaskService(emptyRepository);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USER_TASK_LIST);
        emptyService.set(ADMIN_TASK_LIST);
        Assert.assertEquals(ADMIN_TASK_LIST, emptyService.findAll());
    }

    @Test
    public void findAll() {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        @NotNull final ITaskService emptyService = new TaskService(emptyRepository);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USER_TASK_LIST);
        Assert.assertEquals(USER_TASK_LIST, emptyService.findAll());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findAll("");
        });
        Assert.assertEquals(USER_TASK_LIST, service.findAll(USER_TEST.getId()));
    }

    @Test
    public void findAllComparatorByUserId() {
        @Nullable Comparator comparator = null;
        Assert.assertEquals(USER_TASK_LIST, service.findAll(USER_TEST.getId(), comparator));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable Comparator comparatorInner = null;
            service.findAll("", comparatorInner);
        });
        comparator = NameComparator.INSTANCE;
        Assert.assertEquals(USER_TASK_LIST.stream().sorted(comparator).collect(Collectors.toList()), service.findAll(USER_TEST.getId(), comparator));
    }

    @Test
    public void findAllSortByUserId() {
        @Nullable Sort sort = null;
        Assert.assertEquals(USER_TASK_LIST, service.findAll(USER_TEST.getId(), sort));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable Sort sortInner = null;
            service.findAll("", sortInner);
        });
        sort = Sort.BY_NAME;
        Assert.assertEquals(USER_TASK_LIST.stream().sorted(sort.getComparator()).collect(Collectors.toList()), service.findAll(USER_TEST.getId(), sort.getComparator()));
    }

    @Test
    public void existsById() {
        Assert.assertFalse(service.existsById(""));
        Assert.assertFalse(service.existsById(null));
        Assert.assertFalse(service.existsById(NON_EXISTING_TASK_ID));
        Assert.assertTrue(service.existsById(USER_TASK1.getId()));
    }

    @Test(expected = IdEmptyException.class)
    public void existsByIdByUserId() {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById(null, NON_EXISTING_TASK_ID);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", NON_EXISTING_TASK_ID);
        });
        Assert.assertFalse(service.existsById(USER_TEST.getId(), null));
        Assert.assertFalse(service.existsById(USER_TEST.getId(), ""));
        Assert.assertFalse(service.existsById(USER_TEST.getId(), NON_EXISTING_TASK_ID));
        Assert.assertTrue(service.existsById(USER_TEST.getId(), USER_TASK1.getId()));
    }

    @Test
    public void findOneById() {
        Assert.assertNull(service.findOneById(NON_EXISTING_TASK_ID));
        @Nullable final Task task = service.findOneById(USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1, task);
    }

    @Test
    public void findOneByIdByUserId() {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(USER_TEST.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(USER_TEST.getId(), "");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById(null, USER_TASK1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", USER_TASK1.getId());
        });
        Assert.assertNull(service.findOneById(USER_TEST.getId(), NON_EXISTING_TASK_ID));
        @Nullable final Task task = service.findOneById(USER_TEST.getId(), USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1, task);
    }

    @Test
    public void findOneByIndexByUserId() {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findOneByIndex(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findOneByIndex("", null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(USER_TEST.getId(), null);
        });
        Assert.assertThrows(IllegalArgumentException.class, () -> {
            service.findOneByIndex(USER_TEST.getId(), -1);
        });
        final int index = service.findAll().indexOf(USER_TASK1);
        @Nullable final Task task = service.findOneByIndex(USER_TEST.getId(), index);
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1, task);
    }

    @Test
    public void clear() {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        @NotNull final ITaskService emptyService = new TaskService(emptyRepository);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USER_TASK_LIST);
        emptyService.clear();
        Assert.assertEquals(0, emptyService.getSize());
    }

    @Test
    public void clearByUserId() {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        @NotNull final ITaskService emptyService = new TaskService(emptyRepository);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            emptyService.clear(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            emptyService.clear("");
        });
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USER_TASK1);
        emptyService.add(USER_TASK2);
        emptyService.clear(USER_TEST.getId());
        Assert.assertEquals(0, emptyService.getSize(USER_TEST.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void remove() {
        Assert.assertNull(service.remove(null));
        @Nullable final Task createdTask = service.add(ADMIN_TASK1);
        @Nullable final Task removedTask = service.remove(createdTask);
        Assert.assertNotNull(removedTask);
        Assert.assertEquals(ADMIN_TASK1, removedTask);
        Assert.assertNull(service.findOneById(ADMIN_TASK1.getId()));
    }

    @Test
    public void removeByUserId() {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.remove(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.remove("", null);
        });
        Assert.assertNull(service.remove(ADMIN_TEST.getId(), null));
        @Nullable final Task createdTask = service.add(ADMIN_TASK1);
        @Nullable final Task removedTask = service.remove(ADMIN_TEST.getId(), createdTask);
        Assert.assertEquals(ADMIN_TASK1, removedTask);
        Assert.assertNull(service.findOneById(ADMIN_TEST.getId(), ADMIN_TASK1.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeById() {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById("");
        });
        Assert.assertNull(service.removeById(NON_EXISTING_TASK_ID));
        @Nullable final Task createdTask = service.add(ADMIN_TASK1);
        @Nullable final Task removedTask = service.removeById(ADMIN_TASK1.getId());
        Assert.assertNotNull(removedTask);
        Assert.assertEquals(ADMIN_TASK1, removedTask);
        Assert.assertNull(service.findOneById(ADMIN_TASK1.getId()));
    }

    @Test
    public void removeByIdByUserId() {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById("", null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(USER_TEST.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(USER_TEST.getId(), "");
        });
        Assert.assertNull(service.removeById(ADMIN_TEST.getId(), NON_EXISTING_TASK_ID));
        Assert.assertNull(service.removeById(ADMIN_TEST.getId(), USER_TASK1.getId()));
        @Nullable final Task createdTask = service.add(ADMIN_TASK1);
        @Nullable final Task removedTask = service.removeById(ADMIN_TEST.getId(), createdTask.getId());
        Assert.assertNotNull(removedTask);
        Assert.assertEquals(ADMIN_TASK1, removedTask);
        Assert.assertNull(service.findOneById(ADMIN_TEST.getId(), ADMIN_TASK1.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeByIndex() {
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(-1);
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.removeByIndex(service.getSize());
        });
        @Nullable final Task createdTask = service.add(ADMIN_TASK1);
        final int index = service.findAll().indexOf(createdTask);
        @Nullable final Task removedTask = service.removeByIndex(index);
        Assert.assertNotNull(removedTask);
        Assert.assertEquals(ADMIN_TASK1, removedTask);
        Assert.assertNull(service.findOneById(ADMIN_TASK1.getId()));
    }

    @Test
    public void removeByIndexByUserId() {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeByIndex(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeByIndex("", null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(USER_TEST.getId(), null);
        });
        @Nullable final Task createdTask = service.add(ADMIN_TASK1);
        final int index = service.findAll(ADMIN_TEST.getId()).indexOf(createdTask);
        @Nullable final Task removedTask = service.removeByIndex(ADMIN_TEST.getId(), index);
        Assert.assertNotNull(removedTask);
        Assert.assertEquals(ADMIN_TASK1, removedTask);
        Assert.assertNull(service.findOneById(ADMIN_TEST.getId(), ADMIN_TASK1.getId()));
    }

    @Test
    public void getSize() {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        @NotNull final ITaskService emptyService = new TaskService(emptyRepository);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        Assert.assertEquals(0, emptyService.getSize());
        emptyService.add(ADMIN_TASK1);
        Assert.assertEquals(1, emptyService.getSize());
    }

    @Test
    public void getSizeByUserId() {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        @NotNull final ITaskService emptyService = new TaskService(emptyRepository);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            emptyService.getSize(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            emptyService.getSize("");
        });
        Assert.assertTrue(emptyService.findAll().isEmpty());
        Assert.assertEquals(0, emptyService.getSize(ADMIN_TEST.getId()));
        emptyService.add(ADMIN_TASK1);
        emptyService.add(USER_TASK1);
        Assert.assertEquals(1, emptyService.getSize(ADMIN_TEST.getId()));
    }

    @Test
    public void create() {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create(null, ADMIN_TASK1.getName());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create("", ADMIN_TASK1.getName());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(ADMIN_TEST.getId(), null);
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(ADMIN_TEST.getId(), "");
        });
        @NotNull final Task task = service.create(ADMIN_TEST.getId(), ADMIN_TASK1.getName());
        Assert.assertEquals(task, service.findOneById(ADMIN_TEST.getId(), task.getId()));
        Assert.assertEquals(ADMIN_TASK1.getName(), task.getName());
        Assert.assertEquals(ADMIN_TEST.getId(), task.getUserId());
    }

    @Test
    public void createWithDescription() {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create(null, ADMIN_TASK1.getName(), ADMIN_TASK1.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create("", ADMIN_TASK1.getName(), ADMIN_TASK1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(ADMIN_TEST.getId(), null, ADMIN_TASK1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(ADMIN_TEST.getId(), "", ADMIN_TASK1.getDescription());
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            service.create(ADMIN_TEST.getId(), ADMIN_TASK1.getName(), null);
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            service.create(ADMIN_TEST.getId(), ADMIN_TASK1.getName(), "");
        });
        @NotNull final Task task = service.create(ADMIN_TEST.getId(), ADMIN_TASK1.getName(), ADMIN_TASK1.getDescription());
        Assert.assertEquals(task, service.findOneById(ADMIN_TEST.getId(), task.getId()));
        Assert.assertEquals(ADMIN_TASK1.getName(), task.getName());
        Assert.assertEquals(ADMIN_TASK1.getDescription(), task.getDescription());
        Assert.assertEquals(ADMIN_TEST.getId(), task.getUserId());
    }


    @Test
    public void findAllByProjectId() {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @NotNull final Collection<Task> testCollection = service.findAllByProjectId(null, USER_PROJECT1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @NotNull final Collection<Task> testCollection = service.findAllByProjectId("", USER_PROJECT1.getId());
        });
        @NotNull final Collection<Task> emptyCollection = Collections.emptyList();
        Assert.assertEquals(emptyCollection, service.findAllByProjectId(USER_TEST.getId(), null));
        Assert.assertEquals(emptyCollection, service.findAllByProjectId(USER_TEST.getId(), ""));
        Assert.assertEquals(USER_TASK_LIST, service.findAllByProjectId(USER_TEST.getId(), USER_PROJECT1.getId()));
    }

    @Test
    public void updateById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateById(null, USER_TASK1.getId(), USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateById("", USER_TASK1.getId(), USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateById(USER_TEST.getId(), null, USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateById(USER_TEST.getId(), "", USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateById(USER_TEST.getId(), USER_TASK1.getId(), null, USER_TASK1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateById(USER_TEST.getId(), USER_TASK1.getId(), "", USER_TASK1.getDescription());
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            service.updateById(USER_TEST.getId(), NON_EXISTING_TASK_ID, USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        @NotNull final String name = USER_TASK1.getName() + NON_EXISTING_TASK_ID;
        @NotNull final String description = USER_TASK1.getDescription() + NON_EXISTING_TASK_ID;
        service.updateById(USER_TEST.getId(), USER_TASK1.getId(), name, description);
        Assert.assertEquals(name, USER_TASK1.getName());
        Assert.assertEquals(description, USER_TASK1.getDescription());
    }

    @Test
    public void updateByIndex() {
        @Nullable final Task task = service.findOneById(USER_TASK1.getId());
        Assert.assertNotNull(task);
        final int index = service.findAll().indexOf(task);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateByIndex(null, index, USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateByIndex("", index, USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.updateByIndex(USER_TEST.getId(), null, USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.updateByIndex(USER_TEST.getId(), -1, USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            service.updateByIndex(USER_TEST.getId(), service.getSize(), USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateByIndex(USER_TEST.getId(), index, null, USER_TASK1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateByIndex(USER_TEST.getId(), index, "", USER_TASK1.getDescription());
        });
        @NotNull final String name = USER_TASK1.getName() + NON_EXISTING_TASK_ID;
        @NotNull final String description = USER_TASK1.getDescription() + NON_EXISTING_TASK_ID;
        service.updateByIndex(USER_TEST.getId(), index, name, description);
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(description, task.getDescription());
    }

    @Test
    public void changeTaskStatusById() {
        @NotNull final Status status = Status.COMPLETED;
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeTaskStatusById(null, USER_TASK1.getId(), status);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeTaskStatusById("", USER_TASK1.getId(), status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.changeTaskStatusById(USER_TEST.getId(), null, status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.changeTaskStatusById(USER_TEST.getId(), "", status);
        });
        Assert.assertThrows(NullPointerException.class, () -> {
            service.changeTaskStatusById(USER_TEST.getId(), USER_TASK1.getId(), null);
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            service.changeTaskStatusById(USER_TEST.getId(), NON_EXISTING_TASK_ID, status);
        });
        service.changeTaskStatusById(USER_TEST.getId(), USER_TASK1.getId(), status);
        Assert.assertNotNull(USER_TASK1);
        Assert.assertEquals(status, USER_TASK1.getStatus());
    }

    @Test
    public void changeTaskStatusByIndex() {
        @NotNull final Status status = Status.COMPLETED;
        @Nullable final Task task = service.findOneById(USER_TASK1.getId());
        Assert.assertNotNull(task);
        final int index = service.findAll().indexOf(task);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeTaskStatusByIndex(null, index, status);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeTaskStatusByIndex("", index, status);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.changeTaskStatusByIndex(USER_TEST.getId(), null, status);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.changeTaskStatusByIndex(USER_TEST.getId(), -1, status);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.changeTaskStatusByIndex(USER_TEST.getId(), service.getSize(), status);
        });
        Assert.assertThrows(NullPointerException.class, () -> {
            service.changeTaskStatusByIndex(USER_TEST.getId(), index, null);
        });
        service.changeTaskStatusByIndex(USER_TEST.getId(), index, status);
        Assert.assertEquals(status, task.getStatus());
    }*/

}
